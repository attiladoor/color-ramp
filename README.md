# Color Ramp Generator

When designing graphics hardware it is useful to be able to display various test patterns and
colour ramps. The task is to write a command-line application to generate some of these test
patterns. In the simplest case this program will produce a colour ramp starting with one
colour on one side of the display and changing smoothly to a second colour on the other side.
In the most complex case there will be a different colour in each corner of the display and
each pixel on the display will show the appropriate mix of these four colours.

The program should be invoked by the following command line:
```bash
./ramp display tl tr [bl] [br]
```
where:
* display is the name of the display device
* tl is the top left colour value
* tr is the top right colour value
* bl is the bottom left colour value [optional, defaults to tl]
* br is the bottom right colour value [optional, defaults to tr]

The colour values are specified as 16-bit RGB565 pixels in hex or decimal.

## Run test

The unit tests use the GoogleTest framework. Building and running tests require the properly installed and configured library.
Read more: https://gitlab.com/attiladoor/GoogleTestTutorial

### Build and run test:
```bash
cd ./test/
make
./TestMain
```

## Application

### Building the application

For building the command line application, you need to go the *src* directory and build sources.
```bash
cd ./src
make
```

### Running the application:

The color input can be either standard integer, e.g: 123 or hex: 0x1F or 0x1f

```bash
./ramp monitor 0 0xF
```

As the result, it should print:

```
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 000a 000b 000c 000d 000e 000f
```

In the formula, each color value is calculated as the linear interpolation of corner pixels:

<a href="https://www.codecogs.com/eqnedit.php?latex=C_{tl,x,y}&space;=&space;\frac{((x_{0}&space;-1)&space;-&space;x)&space;*&space;((y_{0}&space;-&space;1)&space;-&space;y))}{(x_{0}&space;-&space;1)&space;*&space;(y_{0}&space;-&space;1)&space;}&space;*&space;C_{tl}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?C_{tl,x,y}&space;=&space;\frac{((x_{0}&space;-1)&space;-&space;x)&space;*&space;((y_{0}&space;-&space;1)&space;-&space;y))}{(x_{0}&space;-&space;1)&space;*&space;(y_{0}&space;-&space;1)&space;}&space;*&space;C_{tl}" title="C_{tl,x,y} = \frac{((x_{0} -1) - x) * ((y_{0} - 1) - y))}{(x_{0} - 1) * (y_{0} - 1) } * C_{tl}" /></a>

<a href="https://www.codecogs.com/eqnedit.php?latex=C_{tr,x,y}&space;=&space;\frac{x&space;*&space;((y_{0}&space;-&space;1)&space;-&space;y))}{(x_{0}&space;-&space;1)&space;*&space;(y_{0}&space;-&space;1)&space;}&space;*&space;C_{tr}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?C_{tr,x,y}&space;=&space;\frac{x&space;*&space;((y_{0}&space;-&space;1)&space;-&space;y))}{(x_{0}&space;-&space;1)&space;*&space;(y_{0}&space;-&space;1)&space;}&space;*&space;C_{tr}" title="C_{tr,x,y} = \frac{x * ((y_{0} - 1) - y))}{(x_{0} - 1) * (y_{0} - 1) } * C_{tr}" /></a>

<a href="https://www.codecogs.com/eqnedit.php?latex=C_{bl,x,y}&space;=&space;\frac{((x_{0}&space;-&space;1)&space;-&space;x)&space;*&space;y}{(x_{0}&space;-&space;1)&space;*&space;(y_{0}&space;-&space;1)&space;}&space;*&space;C_{bl}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?C_{bl,x,y}&space;=&space;\frac{((x_{0}&space;-&space;1)&space;-&space;x)&space;*&space;y}{(x_{0}&space;-&space;1)&space;*&space;(y_{0}&space;-&space;1)&space;}&space;*&space;C_{bl}" title="C_{bl,x,y} = \frac{((x_{0} - 1) - x) * y}{(x_{0} - 1) * (y_{0} - 1) } * C_{bl}" /></a>

<a href="https://www.codecogs.com/eqnedit.php?latex=C_{br,x,y}&space;=&space;\frac{x&space;*&space;y}{(x_{0}&space;-&space;1)&space;*&space;(y_{0}&space;-&space;1)&space;}&space;*&space;C_{br}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?C_{br,x,y}&space;=&space;\frac{x&space;*&space;y}{(x_{0}&space;-&space;1)&space;*&space;(y_{0}&space;-&space;1)&space;}&space;*&space;C_{br}" title="C_{br,x,y} = \frac{x * y}{(x_{0} - 1) * (y_{0} - 1) } * C_{br}" /></a>

<a href="https://www.codecogs.com/eqnedit.php?latex=C_{x,y}&space;=&space;C_{tl,&space;x,y}&space;&plus;&space;C_{tr,&space;x,y}&space;&plus;&space;C_{bl,x,y}&space;&plus;&space;C_{br,x,y}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?C_{x,y}&space;=&space;C_{tl,&space;x,y}&space;&plus;&space;C_{tr,&space;x,y}&space;&plus;&space;C_{bl,x,y}&space;&plus;&space;C_{br,x,y}" title="C_{x,y} = C_{tl, x,y} + C_{tr, x,y} + C_{bl,x,y} + C_{br,x,y}" /></a>

Where *x0* and *y0* are the width and length of canvas.