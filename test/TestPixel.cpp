#include <limits.h>
#include "gtest/gtest.h"
#include "pixel.h"

class PixelTest : public ::testing::Test {
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};

TEST_F(PixelTest,constructor1){

    RGB565 pixel (4, 5, 6);
    EXPECT_EQ(pixel.GetRed(), 4);
    EXPECT_EQ(pixel.GetGreen(), 5);
    EXPECT_EQ(pixel.GetBlue(), 6);
}

TEST_F(PixelTest,constructor2){

    RGB565 pixel (654, 943, 311);
    EXPECT_EQ(pixel.GetRed(), (654 & 0x1F));
    EXPECT_EQ(pixel.GetGreen(), (943 & 0x3F));
    EXPECT_EQ(pixel.GetBlue(), (311 & 0x1F));
}

TEST_F(PixelTest,operator){

    RGB565 pixel1 (4, 5, 6);
    RGB565 pixel2 = pixel1;
    EXPECT_EQ(pixel2.GetRed(), 4);
    EXPECT_EQ(pixel2.GetGreen(), 5);
    EXPECT_EQ(pixel2.GetBlue(), 6);
}

TEST_F(PixelTest,RGBtoInt1){

    uint16_t value1 = RGB565::RGBtoInt(4, 6, 7);
    uint16_t value2 = (4 << 11) | (6 << 5) | (7);
    EXPECT_EQ(value1, value2);
}

TEST_F(PixelTest,RGBtoInt2){

    uint16_t value1 = RGB565::RGBtoInt(654, 943, 311);
    uint16_t value2 = ((654 & 0x1F) << 11) | ((943 & 0x3F) << 5) | (0x1F & 311);
    EXPECT_EQ(value1, value2);
}
