#include <limits.h>
#include "gtest/gtest.h"
#define private public
#include "color_ramp.h"
#include <cstdio>

class ColorRampTest : public ::testing::Test {
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};

TEST_F(ColorRampTest,constructor1){

    TColorRamp c_r = TColorRamp (421, 123, 16, 9);
    const uint16_t* p;
    int w, h;
    c_r.GetOneDimensionalBuffer(p, w, h);
    EXPECT_EQ(w, 16);
    EXPECT_EQ(h, 9);
}

TEST_F(ColorRampTest,constructor2){

    TColorRamp c_r = TColorRamp (421, 123, 16, 9);
    const uint16_t* p;
    int w, h;
    c_r.GetOneDimensionalBuffer(p, w, h);
    EXPECT_EQ(w, 16);
    EXPECT_EQ(h, 9);
}

TEST_F(ColorRampTest, CalculatePixelColor1){

    uint8_t val = TColorRamp::CalculatePixelColor(0x1F, 0, 0 ,0 , 16, 9, 0, 0);
    EXPECT_EQ(val, 0x1F);
    val = TColorRamp::CalculatePixelColor(0x12, 0x1F,0x1F ,0x1F , 16, 9, 0, 0);
    EXPECT_EQ(val, 0x12);
    val = TColorRamp::CalculatePixelColor(0x12, 0x1F,0x10 ,0x15 , 16, 9, 0, 8);
    EXPECT_EQ(val, 0x10);
    val = TColorRamp::CalculatePixelColor(0x1F, 0x08, 0x12 ,0x06, 16, 9, 15, 0);
    EXPECT_EQ(val, 0x08);
    val = TColorRamp::CalculatePixelColor(0x1F, 0x08, 0x12 ,0x06, 16, 9, 15, 8);
    EXPECT_EQ(val, 0x06);
    val = TColorRamp::CalculatePixelColor(0x0, 0x0F, 0 , 0, 16, 9, 8, 0);
    EXPECT_EQ(val, 0x08);
}

TEST_F(ColorRampTest, CalculatePixelColor2){

    uint8_t val = TColorRamp::CalculatePixelColor(0xF, 0, 0 ,0 , 16, 9, 1, 0);
    EXPECT_EQ(val, 0xE);
    val = TColorRamp::CalculatePixelColor(0x18, 0, 0 ,0 , 16, 9, 0, 4);
    EXPECT_EQ(val, 0x0C);
    val = TColorRamp::CalculatePixelColor(0x18, 0, 0x18 ,0 , 16, 9, 0, 4);
    EXPECT_EQ(val, 0x18);
    val = TColorRamp::CalculatePixelColor(0x10, 0x08, 0 ,0 , 16, 9, 8, 0);
    EXPECT_EQ(val, 0xC);
}

/*  @horizontal test
*/

TEST_F(ColorRampTest, BufferTest1){
    TColorRamp c_r = TColorRamp (0x0, 0xF, 16, 9);
    const uint16_t* p;
    int w, h;
    c_r.GetOneDimensionalBuffer(p, w, h); 

    for (int i = 0; i < w; i++) {
       EXPECT_EQ(p[i], i);
    }
}

/*  @vertical test
*/

TEST_F(ColorRampTest, BufferTest2){
    TColorRamp c_r = TColorRamp (0x0, 0, 0xF, 0xF, 9, 16);
    const uint16_t* p;
    int w, h;
    c_r.GetOneDimensionalBuffer(p, w, h); 

    for (int i = 0; i < h; i++) {
        EXPECT_EQ(p[i * w], i);
    }
}
