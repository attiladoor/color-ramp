#include "pixel.h"

#define RED_OFFSET      11
#define GREEN_OFFSET    5
#define BLUE_OFFSET     0

#define BIT_MASK_5      0x1F
#define BIT_MASK_6      0x3F

#define RED_MASK        (BIT_MASK_5 << RED_OFFSET)
#define GREEN_MASK      (BIT_MASK_6 << GREEN_OFFSET)
#define BLUE_MASK       (BIT_MASK_5)

/*  @brief: default constructor of RGB565
*   @param  red:    rgb red value, should be 5bit integer
*   @param  green:  rgb green value, should be 6bit integer
*   @param  blue:   rgb green value, should be 5bit integer
*/

RGB565::RGB565(uint8_t red, uint8_t green, uint8_t blue)
{
    value = (red << RED_OFFSET) | (green << GREEN_OFFSET) | blue;
}

/*  @brief: constructor of RGB565
*   @param  input_color:    RGB565 as uint16_t
*/

RGB565::RGB565(uint16_t input_color)
{
    value = input_color;
}

RGB565::~RGB565() 
{

}

/*  @brief  get function of RGB red value
*   @retval red value as 5bit integer
*/

uint8_t RGB565::GetRed()
{
    return ( (value & RED_MASK) >> RED_OFFSET);
}

/*  @brief  get function of RGB green value
*   @retval green value as 6bit integer
*/

uint8_t RGB565::GetGreen()
{
    return ( (value & GREEN_MASK) >> GREEN_OFFSET);
}

/*  @brief  get function of RGB blue value
*   @retval blue value as 5bit integer
*/

uint8_t RGB565::GetBlue()
{
    return (value & BLUE_MASK);
}

/*  @brief  get function RGB565 value
*   @retval value as uint16_t
*/

uint16_t RGB565::GetValue()
{
    return value;
}

/*  @brief converter function from RGB to RGB565 as uint16_t
*   @retval RGB565 as uint16_t
*/
uint16_t RGB565::RGBtoInt(uint8_t r, uint8_t g, uint8_t b)
{
    RGB565 val = RGB565(r, g, b);
    return val.GetValue();
}
