#include <cstdio>
#include <cassert>
#include <memory>
#include <cstring>

#define W 16
#define H 9

class Display {

    private:
        unsigned short frame_buffer[W * H];
    public:
        Display();
        ~Display();
        bool connect(const char *display_name);
        void get_size(int &width, int &height);
        void draw_raster(int x, int y, const unsigned short *pixels, int width);
};