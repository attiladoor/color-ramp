#include <iostream>
#include <string>
#include "color_ramp.h"
#include "display.h"


using namespace std;
int main(int argc, char *argv[])
{

    string display_name;
    int widht, height;

    TColorRamp* color_ramp; 
    int inputs[4];
    
    if (argc < 4) {
        cout << "Too few arguments, run: ./ramp display tl tr [bl] [br]" << endl;
        return 1;
    } 
    
    for (int  i = 0; i < (argc - 2); i++) {

        try {
            string arg(argv[i + 2]);
            if (arg.substr(0, 2) == "0x") {
                inputs[i] = stoi(arg.substr(2), nullptr, 16);
            } else {
                inputs[i] = stoi(argv[i + 2]);
            }

        } catch (const invalid_argument& ia){
            cerr << "Invalid argument: " << argv[i + 2] << endl;
            return 1;
        }

        if ( (inputs[i] < 0) || (inputs[i] > UINT16_MAX)) {
            cerr << "Error: Invalid color code" << endl;
            return 1;
        }
    }

    Display display;
    display.get_size(widht, height);

    if (argc == 4) {

        color_ramp = new TColorRamp(inputs[0], inputs[1], widht, height);
    } else if (argc == 5) {

        color_ramp = new TColorRamp(inputs[0], inputs[1], inputs[2], widht, height);
    } else if (argc == 6) {

        color_ramp = new TColorRamp(inputs[0], inputs[1], inputs[2], inputs[3], widht, height);
    }

    display.connect(display_name.c_str());
    int w, h; 
    const uint16_t* p;
    color_ramp->GetOneDimensionalBuffer(p, w, h);
    for (int i = 0; i < h; i++) {
        display.draw_raster (0, i,  &p[i*w], w);
    }

    delete color_ramp;
}