#include <cstdint>

#define RGB565_RED_MAX      0x1F
#define RGB565_GREEN_MAX    0x3F
#define RGB565_BLUE_MAX     0x1F


class RGB565 {

    private:
        uint16_t value;
    public:
        RGB565() = delete;
        RGB565(uint8_t red, uint8_t green, uint8_t blue);
        RGB565(uint16_t input_color);
        ~RGB565();

        uint8_t GetRed();
        uint8_t GetGreen();
        uint8_t GetBlue();
        uint16_t GetValue();

        static uint16_t RGBtoInt(uint8_t r, uint8_t g, uint8_t b);

        RGB565 operator=(const RGB565 a) {
            this->value = a.value;
        }
};


