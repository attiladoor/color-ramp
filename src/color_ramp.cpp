#include "color_ramp.h"

/*  @brief  TColorRamp constructor
*/

TColorRamp::TColorRamp(uint16_t top_l, uint16_t top_r, int w, int h) : tl(RGB565(top_l)), tr(RGB565(top_r)), bl(RGB565(top_l)), br(RGB565(top_r)), width(w), height(h)
{
    if ( (w <= 0) || (h <= 0)) {
        throw std::invalid_argument("W and H most be greater than 0");
    }
    InitBuffer();
    FillBuffer();
}

/*  @brief  TColorRamp constructor
*/


TColorRamp::TColorRamp(uint16_t top_l, uint16_t top_r, uint16_t bottom_l, int w, int h) : tl(RGB565(top_l)), tr(RGB565(top_r)), bl(RGB565(bottom_l)), br(RGB565(top_l)), width(w), height(h)
{
    if ( (w <= 0) || (h <= 0)) {
        throw std::invalid_argument("W and H most be greater than 0");
    }
    InitBuffer();
    FillBuffer();
}

/*  @brief  TColorRamp constructor
*/

TColorRamp::TColorRamp(uint16_t top_l, uint16_t top_r, uint16_t bottom_l, uint16_t bottom_r, int w, int h) : tl(RGB565(top_l)), tr(RGB565(top_r)), bl(RGB565(bottom_l)), br(RGB565(bottom_r)), width(w), height(h)
{
    if ( (w <= 0) || (h <= 0)) {
        throw std::invalid_argument("W and H most be greater than 0");
    }
    InitBuffer();
    FillBuffer();
}

/*  @brief  TColorRamp destructor
*   @note   Free buffer memory
*/

TColorRamp::~TColorRamp()
{
    delete[] screen_buffer;
}

/*  @brief  filling image buffer with color ramp values. It requires an initialized buffer
*/

void TColorRamp::FillBuffer()
{
    if (screen_buffer == nullptr) {
        throw std::out_of_range("Unitialized buffer!");
        exit(EXIT_FAILURE);
    }    

    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            
            uint8_t red =   TColorRamp::CalculatePixelColor(tl.GetRed(),   tr.GetRed(),   bl.GetRed(),   br.GetRed(),   width, height, x, y);
            uint8_t green = TColorRamp::CalculatePixelColor(tl.GetGreen(), tr.GetGreen(), bl.GetGreen(), br.GetGreen(), width, height, x, y);
            uint8_t blue =  TColorRamp::CalculatePixelColor(tl.GetBlue(),  tr.GetBlue(),  bl.GetBlue(),  br.GetBlue(),  width, height, x, y);
            if ( (red > RGB565_RED_MAX) || (green > RGB565_GREEN_MAX) || (blue > RGB565_BLUE_MAX)) {
                throw std::out_of_range("Internal error");
            }
            screen_buffer[x + (y * width)] = RGB565::RGBtoInt(red, green, blue);
        }
    }
}

/*  @brief  Buffer initializer function
*   @note   It should be called right from the constructor, it allocates memory by width and height parameters
*           Buffer content is set to zero
*/

void TColorRamp::InitBuffer ()
{
    screen_buffer = new uint16_t [width * height];
    memset(screen_buffer, 0, (height * width * sizeof(uint16_t)));
       
}

/*  @get function of frame buffer
*   @param  buffer_r: reference to return pointer
*   @param  width_r : return width param
*   @param  height_r: return height param
*   @retval none
*/

void TColorRamp::GetOneDimensionalBuffer(const uint16_t* &buffer_r, int &width_r, int &height_r)
{
    if (screen_buffer == nullptr) {
        throw std::invalid_argument("No buffer to return");
    }
    buffer_r = (const uint16_t*) screen_buffer;
    width_r = width;
    height_r = height;
}

/*  @brief  calculates single pixel color according to colors in the corners and the position
*   @param  tl_c:   top left corner color
*   @param  tr_c:   top right corner color
*   @param  bl_c:   bottom left corner color
*   @param  br_c:   bottom right corner color
*   @param  x0:     canvas width
*   @param  y0:     canvas height
*   @param  x:      horizontal position
*   @param  y:      vertical position
*   @retval:        single color value
*/

uint8_t TColorRamp::CalculatePixelColor(uint8_t tl_c, uint8_t tr_c, uint8_t bl_c, uint8_t br_c, int x0, int y0, int x, int y)
{
    double x0_d = (double(x0) - 1);
    double y0_d = (double(y0) - 1);
    double x_d  = double(x);
    double y_d  = double(y);

    double tl_multiplier = (x0_d - x_d) * (y0_d - y_d) / (x0_d * y0_d);
    double tr_multiplier = (x_d)        * (y0_d - y_d) / (x0_d * y0_d);
    double bl_multiplier = (x0_d - x_d) * (y_d)        / (x0_d * y0_d);
    double br_multiplier = (x_d)        * (y_d)        / (x0_d * y0_d);

    double color = round((tl_multiplier * (double) tl_c) + (tr_multiplier * (double) tr_c) + (bl_multiplier * (double) bl_c) + (br_multiplier * (double) br_c));
    return (uint8_t) color;
}
