#include <cstdlib>
#include <cstring>
#include <cmath>
#include <stdexcept>
#include "pixel.h"


class TColorRamp {

    public:
        TColorRamp() = delete;
        TColorRamp(uint16_t top_l, uint16_t top_r, int w, int h);
        TColorRamp(uint16_t top_l, uint16_t top_r, uint16_t bottom_l, int w, int h);
        TColorRamp(uint16_t top_l, uint16_t top_r, uint16_t bottom_l, uint16_t bottom_r, int w, int h);
        ~TColorRamp();

        void GetOneDimensionalBuffer(const uint16_t* &buffer_r, int &width_r, int &height_r);

    private:
        RGB565 tl, tr, bl, br;
        int width, height;
        uint16_t* screen_buffer = nullptr;
        void FillBuffer();
        void InitBuffer ();
        static uint8_t CalculatePixelColor(uint8_t tl_c, uint8_t tr_c, uint8_t bl_c, uint8_t br_c, int x0, int y0, int x, int y);
};
